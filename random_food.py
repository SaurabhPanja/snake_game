import pygame
import random
# from init_screen import init_screen
def random_food(screen, screen_width, screen_height):
    # generate random food on screen
    # screen = init_screen()
    x = random.randint(0, screen_width + 1)
    y = random.randint(0, screen_height + 1)
    pygame.draw.rect(screen, (50,205,50), pygame.Rect(x, y, 5, 5))
    return x, y


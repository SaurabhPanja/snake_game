import pygame
from init_screen import init_screen

def snake(screen, x, y, x_direction, y_direction):
    # snake = a rectangle height 10 width 10
    # screen = init_screen()
    # print(x, y, x_direction, y_direction)
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x, y, 10, 10))
    if y_direction == 0 and x_direction == 0.1:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x - 10, y, 10 ,10))
    elif y_direction == 0.1 and x_direction == 0:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y - 10, 10 , 10))
    elif y_direction == -0.1 and x_direction == 0:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y + 10, 10 ,10))
    elif y_direction == 0 and x_direction == -0.1:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x + 10, y, 10 , 10))
    # moves accross all axis on key press
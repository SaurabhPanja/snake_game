import pygame
from init_screen import init_screen
from snake import snake
from random_food import random_food

def game_loop():
    screen_width, screen_height  = 500, 500
    screen = init_screen(screen_width, screen_height)
    screen.fill((255, 255, 255))
    snake_x, snake_y, x_direction, y_direction = 0, 0, 0, 0
    done = False
    pygame.display.set_caption('Snake game not snakes and ladders')
    food_x, food_y = random_food(screen, screen_width, screen_height)
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    y_direction = -0.1
                    x_direction = 0
                if event.key == pygame.K_DOWN:
                    y_direction = 0.1
                    x_direction = 0
                if event.key == pygame.K_LEFT:
                    x_direction = -0.1
                    y_direction = 0
                if event.key == pygame.K_RIGHT:
                    x_direction = 0.1
                    y_direction = 0
        snake_x += x_direction
        snake_y += y_direction
        print(int(snake_x), food_x, int(snake_y), food_y)
        if int(snake_x) in range(food_x - 5,food_x + 5) and int(snake_y) in range(food_y - 5, food_y + 5):
            # print("executed")
            screen.fill((255, 255, 255))
            food_x, food_y = random_food(screen, screen_width, screen_height)
            # increase snake width
        # pygame.draw.rect(screen, (50,205,50), pygame.Rect(123, 123, 10, 10))
        snake(screen, snake_x, snake_y, x_direction, y_direction)
        pygame.display.flip()
    
